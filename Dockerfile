FROM ubuntu:18.04

ENV TERM screen-256color
ENV DEBIAN_FRONTEND noninteractive


RUN apt-get update && apt-get install -y \
	git \
	vim

RUN cd /opt && \
    git clone https://gitlab.com/SparrowOchon/neovim-setup.git && \
    ./neovim-setup/install.sh
