#!/bin/bash
#================================================================================================
#
# FILENAME :        ansible-install.sh
#
# DESCRIPTION : Installation script to be executed via ansible
#		https://gitlab.com/SparrowOchon/neovim-setup/
#
# USAGE:
#		ansible-install.sh <full | compiled | web | min>
#
#
# AUTHOR :   Network Silence        START DATE :    23 April 2019
#
#
# LICENSE :
#	Show don't Sell License Version 1
#   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>
#
#	Copyright (c) 2019, Network Silence
#		All rights reserved.
#
#
#
#
#===============================================================================================
PASS="" #Password of sudoer to avoid asking multiple times
installDir=/home/$USER/.config/nvim
nvimSyntax=$installDir/syntax

function main(){
	if [[ $# -ne 1 ]]; then
		invalid_input
	fi

	case $1 in
		"full")
			general_config
			neovim_plugin_install
			web_languages
			compiled_languages
			;;
		"web")
			general_config
			neovim_plugin_install
			web_languages
			;;
		"compiled")
			general_config
			neovim_plugin_install
			compiled_languages
			;;
		"min")
			general_config
			neovim_plugin_install
			;;
		*)
			invalid_input
			;;
	esac
}

function neovim_plugin_install(){
	# Setup Vim plug
	curl -fLo $installDir/autoload/plug.vim --create-dirs \
		https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	nvim +PlugInstall +qall	# Install all Plugins

}
function invalid_input(){
	echo "USAGE: $0 < full | web | compiled | min >\n"
	echo "Select a singular installation type"
	exit 1
}
function web_languages(){

    # Install XO for JS
    npm install --global xo

	# Install Hadolint for Dockerfile
    https://api.github.com/repos/hadolint/hadolint/releases/latest

	# Install checkov and tflint for Terraform
    pip3 install checkov
    curl -s https://raw.githubusercontent.com/terraform-linters/tflint/master/install_linux.sh | bash


    # Install lint-openapi for openapi
    npm install --global ibm-openapi-validator

    # sqlfmt,
    pip3 install sqlparse
    
    # nginxbeautifier,
    npm install --global nginxbeautifier
    
    # terraform,
    curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
    apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
    apt-get update -y
	apt-get install terraform -y

	# Linters
	npm install --global csslint	# CSS Linter
	npm install --global prettier

	# Compile/Install Autocompleting engine to use Clang LSP
	/home/$USER/.config/nvim/plugged/YouCompleteMe/install.py --ts-completer
}
function compiled_languages(){

	# Install luaformatter
	apt install build-essential libreadline-dev unzip
	curl -R -O http://www.lua.org/ftp/lua-5.4.4.tar.gz
	tar -zxf lua-5.4.4.tar.gz
	cd lua-5.4.4
	make linux test
	sudo make install
	wget https://luarocks.org/releases/luarocks-3.9.0.tar.gz
	tar zxpf luarocks-3.9.0.tar.gz
	cd luarocks-3.9.0
	./configure --with-lua-include=/usr/local/include
	make
	make install
	luarocks install --server=https://luarocks.org/dev luaformatter


	# Setup Zeek config dont install it
    https://api.github.com/repos/zeek/zeek/releases/latest

    # Setup NVCC for Cuda
    apt-get install nvidia-cuda-toolkit

    # Install checkmake for Makefiles
    https://api.github.com/repos/mrtazz/checkmake/releases/latest

    # Install solhint and Ethlint for Solidity
    npm install --global solhint
    npm install --global ethlint


    # Install cmakelint for Cmake
    pip3 install cmakelint

    # Install oelint-adv for BitBake
    pip3 install oelint_adv

    # Install ansible-lint for ansible
    pip3 install ansible-lint

    # Install Cargo for rust
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    rustup component add rustfmt
    export=$PATH:~/.cargo/bin

    # Install selene for Lua
    cargo install selene

	# Not using plugin manager as this breaks neovim highlighting unless explicitly appended
	git clone https://github.com/bfrg/vim-cpp-modern # Modern CPP syntax Highlighting C++11/C++14/C++17 and Some from C++20
	cat vim-cpp-modern/after/syntax/c.vim >> $nvimSyntax/c.vim
	cat vim-cpp-modern/after/syntax/cpp.vim >> $nvimSyntax/cpp.vim
	rm -R vim-cpp-modern

	/home/$USER/.config/nvim/plugged/YouCompleteMe/install.py --go-completer
	/home/$USER/.config/nvim/plugged/YouCompleteMe/install.py --clangd-completer
}
function general_config(){
	if [[ -f $install/autoload/plug.vim ]]; then
		echo "General Config already installed"
		return 1
	fi

    # Install sql-lint for SQL (not NPM)
    npm install --global sql-lint
    pip3 install --user yamllint
    npm install --global eslint

	mkdir -p $installDir
	mkdir -p $nvimSyntax
	cp init.vim $installDir/
	cp -R config/ $installDir/
}
main "$@"
