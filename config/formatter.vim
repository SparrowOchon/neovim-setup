augroup fmt
	autocmd!
	autocmd BufWritePre * undojoin | Neoformat
	autocmd BufWritePre * StripWhitespace
augroup END


augroup detect
	autocmd!
	autocmd BufNewFile,BufRead *.vue,*.wpy set filetype=vue
	autocmd BufNewFile,BufRead *.blade.php set filetype=blade
augroup END


" For some reason these settings dont work
let g:better_whitespace_enabled=1
" let g:strip_whitespace_on_save=1

" Put all standard C and C++ keywords under Vim's highlight group `Statement`
" (affects both C and C++ files)
let g:cpp_simple_highlight = 1
" Enable highlighting of named requirements (C++20 library concepts)
let g:cpp_named_requirements_highlight = 1


let g:neoformat_cpp_clangformat ={
			\'exe':'clang-format',
			\'args':['-style=file'],
			\'stdin':1
			\}

let g:neoformat_c_clangformat ={
			\'exe':'clang-format',
			\'args':['-style=file'],
			\'stdin':1
			\}

let g:neoformat_adruino_clangformat ={
			\'exe':'clang-format',
			\'args':['-style=file'],
			\'stdin':1
			\}

let g:neoformat_enabled_python = ['black']
let g:neoformat_enabled_lua = ['luaformatter']
let g:neoformat_enabled_cpp = ['clangformat']
let g:neoformat_enabled_c = ['clangformat']
let g:neoformat_enabled_arduino = ['clangformat']
let g:neoformat_enabled_java = ['clangformat']
let g:neoformat_enabled_go = ['gofmt', 'goimports']
let g:neoformat_enabled_xhtml = ['tidy']
let g:neoformat_enabled_xml = ['tidy']
let g:neoformat_enabled_json = ['prettier']
let g:neoformat_enabled_jsonc = ['prettier']
let g:neoformat_enabled_nginx = ['nginxbeautifier']
let g:neoformat_enabled_yaml = ['prettier']
let g:neoformat_enabled_terraform = ['terraform']
let g:neoformat_enabled_sql = ['sqlformat']
let g:neoformat_enabled_solidity = ['prettier']
let g:neoformat_enabled_rust = ['rustfmt']
let g:neoformat_enabled_proto = ['clangformat']
let g:neoformat_enabled_html = ['prettier']
let g:neoformat_enabled_css = ['prettier']
let g:neoformat_enabled_javascript = ['prettier']
let g:neoformat_enabled_blade = ['prettier']
let g:neoformat_enabled_vue = ['prettier']
let g:neoformat_enabled_markdown = ['prettier']
let g:mkdp_auto_start = 1
let g:mkdp_auto_close = 1
let g:mkdp_refresh_slow = 1
