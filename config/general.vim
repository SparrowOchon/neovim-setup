" Allow Xterm Keys
if &term =~ '^screen'
    " tmux will send xterm-style keys when its xterm-keys option is on
    execute "set <xUp>=\e[1;*A"
    execute "set <xDown>=\e[1;*B"
    execute "set <xRight>=\e[1;*C"
    execute "set <xLeft>=\e[1;*D"
endif


" BLOCK MOVEMENT
set colorcolumn=120
nnoremap <F3> :buffers<CR>:buffer<Space>
nnoremap <C-S-down> :m .+1<CR>==
nnoremap <C-S-up> :m .-2<CR>==
vnoremap <C-S-down> :m '>+1<CR>gv=gv
vnoremap <C-S-up> :m '<-2<CR>gv=gv

" VIRTUAL MULTI HIGHLIGHT
nmap <S-Up> v<Up>
nmap <S-Down> v<Down>
nmap <S-Left> v<Left>
nmap <S-Right> v<Right>
nmap <S-Home> v<Home>
nmap <S-End> v<End>
vmap <S-Up> <Up>
vmap <S-Down> <Down>
vmap <S-Left> <Left>
vmap <S-Right> <Right>
imap <S-Home> <Esc>v<Home>
imap <S-End> <Esc>v<End>
imap <S-Up> <Esc>v<Up>
imap <S-Down> <Esc>v<Down>
imap <S-Left> <Esc>v<Left>
imap <S-Right> <Esc>v<Right>

" CLIPBOARD VIM
function! ClipboardYank()
	call system('xclip -i -selection clipboard', @@)
endfunction
function! ClipboardPaste()
	let @@ = system('xclip -o -selection clipboard')
	set nopaste
endfunction

command -nargs=0 -bar Update if &modified
			\|    if empty(bufname('%'))
				\|        browse confirm write
				\|    else
					\|        confirm write
					\|    endif
					\|endif
nnoremap <silent> <C-s> :<C-u>Update<CR>
vmap <C-c> y y:call ClipboardYank()<cr><Esc>i
vmap <C-x> d d:call ClipboardYank()<cr><Esc>i
vmap <C-v> p :call ClipboardPaste()<cr>i
imap <C-v> <Esc>p :call ClipboardPaste()<cr>i
imap <C-z> <Esc>ui
vnoremap <silent> y y:call ClipboardYank()<cr>
vnoremap <silent> d d:call ClipboardYank()<cr>
nnoremap <silent> p :call ClipboardPaste()<cr>

" NAVIGATION WITH TMUX
" Create horizontal splits below the current window
set splitbelow
set splitright
" Tmux vim integration
let g:tmux_navigator_no_mappings = 1
let g:tmux_navigator_save_on_switch = 1

" Move between splits with ctrl+h,j,k,l
nnoremap <silent> <c-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <c-j> :TmuxNavigateUp<cr>
nnoremap <silent> <c-k> :TmuxNavigateDown<cr>
nnoremap <silent> <c-l> :TmuxNavigateRight<cr>
nnoremap <silent> <c-\> :TmuxNavigatePrevious<cr>

" TARGBAR Toggling
let g:tagbar_right=1
"let g:tagbar_vertical = 25
let NERDTreeWinPos = 'left'
let NERDTreeMinimalUI = 1
nnoremap <f2> :NERDTreeToggle <CR>
nnoremap <f4> :TagbarToggle <CR>

" COMMENTING HIGHLIGHTED BLOCKS
let s:comment_map = {
			\   "c": '\/\/',
			\   "cpp": '\/\/',
			\   "go": '\/\/',
			\   "lua": '--',
			\   "java": '\/\/',
			\   "javascript": '\/\/',
			\   "scala": '\/\/',
			\   "php": '\/\/',
			\   "python": '#',
			\   "ruby": '#',
			\   "rust": '\/\/',
			\   "sh": '#',
			\   "desktop": '#',
			\   "fstab": '#',
			\   "conf": '#',
			\   "profile": '#',
			\   "bashrc": '#',
			\   "bash_profile": '#',
			\   "mail": '>',
			\   "eml": '>',
			\   "bat": 'REM',
			\   "ahk": ';',
			\   "vim": '"',
			\   "tex": '%',
			\ }

function! ToggleComment()
	if has_key(s:comment_map, &filetype)
		let comment_leader = s:comment_map[&filetype]
		if getline('.') =~ "^\\s*" . comment_leader . " "
			" Uncomment the line
			execute "silent s/^\\(\\s*\\)" . comment_leader . " /\\1/"
		else
			if getline('.') =~ "^\\s*" . comment_leader
				" Uncomment the line
				execute "silent s/^\\(\\s*\\)" . comment_leader . "/\\1/"
			else
				" Comment the line
				execute "silent s/^\\(\\s*\\)/\\1" . comment_leader . " /"
			end
		end
	else
		echo "No comment leader found for filetype"
	end
endfunction


nnoremap <C-/> :call ToggleComment()<cr>
vnoremap <C-S-/> :call ToggleComment()<cr>
