" We disable ALE fixers because Neoformat does a faster and better job
let g:ale_completion_enabled = 0
let g:ale_sign_warning = '▲'
let g:ale_sign_error = '✗'
let g:ale_set_quickfix = 0
let g:ale_echo_msg_error_str = 'Err'
let g:ale_echo_msg_warning_str = 'Warn'
let g:ale_echo_msg_format = '[%severity%] %s'
let g:ale_set_highlights = 0
let g:ale_linters_explicit = 1
let g:ale_fix_on_save = 0
let g:ale_completion_enabled = 0
let b:ale_warn_about_trailing_whitespace = 1
let g:ale_cpp_clangtidy_checks = ['bugprone-*', 'modernize-*', 'clang-analyzer-*', 'performance-*']
let g:ale_c_clangtidy_checks=['bugprone-*', 'modernize-*', 'performance-*', 'clang-analyze-*' ]
let g:ale_php_phpmd_ruleset = 'codesize,design,unusedcode'
let g:ale_c_gcc_options='-std=c17 -Wall'
let g:ale_cpp_gcc_options='-std=c++1z -Wall'
let g:ale_c_flawfinder_error_severity = 4

" Run multiple linters on specific files
let b:ale_linter_aliases = {
			\'php': ['html', 'javascript', 'css'],
			\'html': ['javascript', 'css','html']
			\}

" Linters we want to use
let b:ale_linters = {
			\'ansible': ['ansible-lint'],
			\'rust': ['cargo'],
			\'cmake': ['cmake-lint'],
			\'yaml': ['yamllint'],
			\'sql': ['sqllint'],
			\'json': ['eslint'],
			\'json5': ['eslint'],
			\'jsonc': ['eslint'],
			\'graphql': ['eslint'],
			\'fortran': ['gcc'],
			\'dockerfile': ['hadolint'],
			\'cuda': ['nvcc'],
			\'make': ['checkmake'],
			\'solidity': ['solhint', 'solium'],
			\'terraform': ['checkov', 'tflint'],
			\'bitbake': ['oelint-adv'],
			\'mathlab': ['mlint'],
			\'openapi': ['yamllint', 'lint-openapi'],
			\'python': ['pylint'],
			\'cpp': ['clangtidy','flawfinder','gcc'],
			\'c': ['clangtidy','flawfinder', 'gcc'],
			\'lua': ['selene'],
			\'javascript': ['xo','tsserver'],
			\'typescript': ['xo','tsserver'],
			\'go': ['golangci-lint'],
			\'html':['tidy'],
			\'css':['csslint'],
			\'sh':['shellcheck']
			\}

" Disable Fixers.
let b:ale_fixers_enabled = 0
autocmd FileType html setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
autocmd FileType java set omnifunc=javacomplete#Complete
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
autocmd FileType css set omnifunc=csscomplete#CompleteCSS
"autocmd FileType ruby setlocal omnifunc=rubycomplete#Complete
" use syntax complete if nothing else available
if has("autocmd") && exists("+omnifunc")
	autocmd Filetype *
				\ if &omnifunc == "" |
				\   setlocal omnifunc=syntaxcomplete#Complete |
				\ endif
endif
