call plug#begin()
Plug 'w0rp/ale'				" Linter/Formater
Plug 'itchyny/lightline.vim'		" Status Lines
Plug 'maximbaz/lightline-ale'		" Ale Integration for Status
Plug 'Valloric/YouCompleteMe'		" AutoComplete/Intellisense
Plug 'SirVer/ultisnips'			" Snippets
Plug 'jiangmiao/auto-pairs'		" AutoPairing
Plug 'ludovicchabant/vim-gutentags'	" Ctag Generator
Plug 'majutsushi/tagbar'		" TagBar
Plug 'scrooloose/nerdtree'		" Nerdtree
Plug 'morhetz/gruvbox'			" Theme
Plug 'sbdchd/neoformat'			" Autoformater / ALEFixer way too slow
Plug 'honza/vim-snippets'		" Snippets Presets
Plug 'ntpeters/vim-better-whitespace' 	" Strip White Space on Save
Plug 'PhilRunninger/nerdtree-buffer-ops' " Highlight Open Buffers in NerdTree
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }
Plug 'kkoomen/vim-doge'			" Documentation Helper
call plug#end()

set background=dark
colorscheme gruvbox
set number
set mouse=a

source $HOME/.config/nvim/config/general.vim
source $HOME/.config/nvim/config/statusbar.vim
source $HOME/.config/nvim/config/autocomplete.vim
source $HOME/.config/nvim/config/snips.vim
source $HOME/.config/nvim/config/linter.vim
source $HOME/.config/nvim/config/formatter.vim
