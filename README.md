# Neovim config

#### My Custom Neovim config for my personal needs keeping it async and minimalistic (Also less random shortcuts).

## Usage:
            
to install this simply execute the `install.sh` script followed by the type of installations you would like based on
the Autocompletion and languages you want to work with.

- `compiled`: C++/C, Golang, Java, Rust, Lua, Cmake, Make, Bitbake, Solidity, Fortran, Ansible, Zeek
- `web`: Html, xHtml, Json, CSS, JavaScript, TypeScript, Sql, Graphql, Openapi, Terraform, Docker
- `min`: Python, XML, Markdown, Shell, Yaml,
- `full`: The following will install everything from all 3 other settings.

NOTE: All installations will automatically install all general settings from the `min` version as well.

## Plugins Included:

- [YouCompleteMe AutoCompleter](https://github.com/Valloric/YouCompleteMe)
- [ALE Linter](https://github.com/w0rp/ale)
- [Lightline](https://github.com/itchyny/lightline.vim)
- [Lightline-ale](https://github.com/maximbaz/lightline-ale)
- [UltiSnips](https://github.com/SirVer/ultisnips)
- [AutoPair](https://github.com/jiangmiao/auto-pairs)
- [Vim-gutentags](https://github.com/ludovicchabant/vim-gutentags)
- [Tagbar](https://github.com/majutsushi/tagbar)
- [Gruvbox theme](https://github.com/morhetz/gruvbox)
- [Neoformat Formatter](https://github.com/sbdchd/neoformat)
- [Honza's Snippet collection](https://github.com/honza/vim-snippets)
- [Better Whitepace Highlighting](https://github.com/ntpeters/vim-better-whitespace)
- [Nerdtree Buffer Operations](https://github.com/PhilRunninger/nerdtree-buffer-ops)
- [Markdown Preview](https://github.com/iamcco/markdown-preview.nvim)
- [Documentation Generator](https://github.com/kkoomen/vim-doge)

## Languages Supported:

- C/C++ (AutoComplete,Linter)
- Golang (AutoComplete,Linter)
- Rust (AutoComplete,Linter)
- JavaScript (AutoComplete,Linter)
- TypeScript (AutoComplete,Linter)
- Lua (AutoComplete,Linter)
- Python (AutoComplete,Linter)

![exampleimage](image.png)

| Command Shortcut      |                    Description                    |
| --------------------- | :-----------------------------------------------: |
| `Ctrl + /`            |     Add comment infront of highlighted block      |
| `Ctrl + Shift + /`    | Remove comments from infront of highlighted block |
| `Ctrl-Space`          |    Complete Highlighted Snippet in pumvisible     |
| `Alt-e`               |          Move End pair around next word           |
| `F2`                  |            Toggle NerdTree and Tagbar             |
| `F3`                  |                    Buffer List                    |
| `\d`                  |              Documentation Generator              |
| `Ctrl + c`            |                       Copy                        |
| `Ctrl + x`            |                        Cut                        |
| `Ctrl + v`            |                       Paste                       |
| `Ctrl + z`            |                       Undo                        |
| `Ctrl + r`            |                       Redo                        |
| `Ctrl + s`            |               Save(In normal mode)                |
| `Shift + Up`          |               Highlight Upper Line                |
| `Shift + Down`        |               Highlight Lower Line                |
| `Shift + Left`        |         Highlight Characters to the left          |
| `Shift + Right`       |         Highlight Characters to the Right         |
| `Shift + Home`        |   Highlight Text till the beginning of the line   |
| `Shift + End`         |      Highlight Text till the end of the line      |
| `Shift + M`           |               Next Value in Snippet               |
| `Shift + N`           |               Prev Value in Snippet               |
| `Ctrl + Shift + Up`   |          Move Highlighted Block up by 1           |
| `Ctrl + Shift + Down` |         Move Highlighted Block down by 1          |