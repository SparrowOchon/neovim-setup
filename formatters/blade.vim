function! neoformat#formatters#blade#enabled() abort
	    return ['prettier']
endfunction
function! neoformat#formatters#blade#prettier() abort
    return {
        \ 'exe': 'prettier',
        \ 'args': ['--stdin', '--stdin-filepath', '"%:p"', '--parse=', 'html'],
        \ 'stdin': 1,
	\}
endfunction 

