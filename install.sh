#!/bin/bash
#================================================================================================
#
# FILENAME :        install.sh
#
# DESCRIPTION : Installation script for the neovim config found at
#		https://gitlab.com/SparrowOchon/neovim-setup/
#
# USAGE:
#		install.sh <full | compiled | web | min>
#
#
# AUTHOR :   Network Silence        START DATE :    23 April 2019
#
#
# LICENSE :
#	Show don't Sell License Version 1
#   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>
#
#	Copyright (c) 2019, Network Silence
#		All rights reserved.
#
#
#
#
#===============================================================================================
PASS="" #Password of sudoer to avoid asking multiple times
installDir=/home/$USER/.config/nvim
nvimSyntax=$installDir/syntax

function main(){
	if [[ $(id -u) = 0 ]]; then
	   echo "Do not run this entire script as sudo." >&2
		exit 1
	elif [[ $# -ne 1 ]]; then
		invalid_input
	fi

	case $1 in
		"full")
			general_config
			neovim_plugin_install
			web_languages
			compiled_languages
			;;
		"web")
			general_config
			neovim_plugin_install
			web_languages
			;;
		"compiled")
			general_config
			neovim_plugin_install
			compiled_languages
			;;
		"min")
			general_config
			neovim_plugin_install
			;;
		*)
			invalid_input
			;;
	esac
}

function neovim_plugin_install(){
	# Setup Vim plug
	curl -fLo $installDir/autoload/plug.vim --create-dirs \
		https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	nvim +PlugInstall +qall	# Install all Plugins

}
function invalid_input(){
	echo "USAGE: $0 < full | web | compiled | min >\n"
	echo "Select a singular installation type"
	exit 1
}
function get_sudoer_pass(){
	while [[ $PASS == "" ]]; do
		sudo -k
		echo -n "Enter the sudo password:"
		read temppass
		echo $temppass | sudo -S echo testpass &> /dev/null
		if ! [ "$(sudo -n echo testpass 2>&1)" == "testpass" ]; then
			echo "Incorrect password was entered"
		else
			PASS=$temppass
		fi
	done
}
function web_languages(){
	echo $PASS | sudo -S apt-get install nodejs npm composer phpmd -y

	echo $PASS | sudo -S git clone https://github.com/jwalton512/vim-blade.git
	echo $PASS | sudo -S cp vim-blade/syntax/blade.vim $nvimSyntax/
	echo $PASS | sudo -S rm -R vim-blade

	echo $PASS | sudo -S git clone https://github.com/posva/vim-vue.git
	echo $PASS | sudo -S cp vim-vue/syntax/vue.vim $nvimSyntax/
	echo $PASS | sudo -S rm -R vim-vue

	# Linters
	echo $PASS | sudo -S npm install --global csslint	# Css Linter
	composer require --dev phpstan/phpstan # Php Code Analysis Tool

	echo $PASS | sudo -S npm install --global --save-dev --save-exact prettier
	composer require friendsofphp/php-cs-fixer # Php Formatter

	export PATH="$PATH:$HOME/.config/composer/vendor/bin" # Add Composer to path to be able to use Modules in Vim
	echo $PASS | sudo -S cp formatters/blade.vim $installDir/plugged/neoformat/autoload/neoformat/formatters/
	echo $PASS | sudo -S cp formatters/vue.vim $installDir/plugged/neoformat/autoload/neoformat/formatters/

	# Compile/Install Autocompleting engine to use Clang LSP
	/home/$USER/.config/nvim/plugged/YouCompleteMe/install.py --ts-completer
}
function compiled_languages(){
	echo $PASS | sudo -S apt-get install ctags golang clang-tools-8 clang-tidy cmake gcc cppcheck clang-format flawfinder -y

	go get -u github.com/golangci/golangci-lint/cmd/golangci-lint


	# Not using plugin manager as this breaks neovim highlighting unless explicitly appended
	echo $PASS | sudo -S git clone https://github.com/bfrg/vim-cpp-modern # Modern CPP syntax Highlighting C++11/C++14/C++17 and Some from C++20
	echo $PASS | sudo -S cat vim-cpp-modern/after/syntax/c.vim >> $nvimSyntax/c.vim
	echo $PASS | sudo -S cat vim-cpp-modern/after/syntax/cpp.vim >> $nvimSyntax/cpp.vim
	echo $PASS | sudo -S rm -R vim-cpp-modern

	/home/$USER/.config/nvim/plugged/YouCompleteMe/install.py --go-completer
	/home/$USER/.config/nvim/plugged/YouCompleteMe/install.py --clangd-completer
}
function general_config(){
	if [[ -f $install/autoload/plug.vim ]]; then
		echo "General Config already installed"
		return 1
	fi
	get_sudoer_pass
	echo $PASS | sudo -S apt-get remove --purge vim*
	echo $PASS | sudo -S apt-get install build-essential neovim python3-dev python3 shellcheck \
		git pylint python3-pip python3-neovim tidy tmux python-dev curl -y

	mkdir -p $installDir
	mkdir -p $nvimSyntax
	echo $PASS | sudo -S cp init.vim $installDir/
	echo $PASS | sudo -S cp -R config/ $installDir/

	# Formatting
	echo $PASS | sudo -S pip3 install black bandit radon # Python Code Formatter pep8

	# Autostart Tmux in Bash Instances
	cat <<- 'EOF' >> /home/$USER/.bashrc
		# Uncomment to Kill Tmux Instance on Close
		#function close_tmux {
			#tmux kill-session -t $(tmux ls 2> /dev/null | grep -v attached | head -1 | cut -d : -f 1)
		#}
		#trap close_tmux EXIT

		# Code to auto-reattach to Tmux Instance or start a new one
		case $- in
			*i*)
				if command -v tmux>/dev/null; then
					if [[ ! $TERM =~ screen ]] && [[ -z $TMUX ]]; then
						if tmux ls 2> /dev/null | grep -q -v attached; then
							exec tmux attach -t $(tmux ls 2> /dev/null | grep -v attached | head -1 | cut -d : -f 1)
						else
							exec tmux
						fi
					fi
				fi
			;;
		esac
	EOF

	# Tmux config and terminal colors
	echo $PASS | sudo -S git clone https://gitlab.com/SparrowOchon/dotfiles.git
	echo $PASS | sudo -S mv dotfiles/tmux.conf ~/.tmux.conf
	if [[ $(echo $XDG_CURRENT_DESKTOP) == "XFCE" ]];then
		cat dotfiles/terminalrc /home/$USER/.config/xfce4/terminal/terminalrc
	fi
	echo $PASS | sudo -S rm -R dotfiles

	# Install Tmux yank plugin to copy/paste in Tmux instance
	echo $PASS | sudo -S git clone https://github.com/tmux-plugins/tmux-yank /home/$USER/.tmux-plugins/
	tmux source-file ~/.tmux.conf
}
main "$@"
